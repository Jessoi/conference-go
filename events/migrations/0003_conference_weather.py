# Generated by Django 4.0.3 on 2023-09-21 18:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_image_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='weather',
            field=models.TextField(null=True),
        ),
    ]
