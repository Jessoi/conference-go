import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_image(city, state):
    res = requests.get(
                    f'https://api.pexels.com/v1/search?query={city}+{state}',
                    headers={"Authorization": PEXELS_API_KEY}
                )
    data = json.loads(res.text)
    try:
        return data["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # res = requests.get(f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=5&appid={OPEN_WEATHER_API_KEY}')
    # data = json.loads(res.text)
    # lon = data[0]["lon"]
    # lat = data[0]["lat"]
    # res = requests.get(
    #             f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial')
    # data = json.loads(res.text)

    geo_url = 'http://api.openweathermap.org/geo/1.0/direct'
    geo_params = {
        "q": f'{city},{state},US',
        "appid": OPEN_WEATHER_API_KEY
    }

    response = requests.get(geo_url, params=geo_params)
    geo_content = json.loads(response.content)
    lon = geo_content[0]["lon"]
    lat = geo_content[0]["lat"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    
    response = requests.get(weather_url, params=weather_params)
    weather_content = json.loads(response.content)
    try:
        weather_data = {
            "temp": weather_content["main"]["temp"], "description": weather_content["weather"][0]["description"]
        }
        return weather_data
    except (KeyError, IndexError):
        return {"weather": None}

    # try:
    #     return {"temp": data["main"]["temp"], "description": data["weather"][0]["description"]}
    # except (KeyError, IndexError):
    #     return {"weather": None}